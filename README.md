# 🖼️ Go gallery

Go gallery is a simple web gallery written in [golang](https://golang.org) to browse a folder hierarchy containing pictures. It displays them in a nice and elegant way.
It groups folders by month and day based on their names and displays them chronologically.

A directory containing subfolders is displayed as follows:
![Root folder](/docs/folders.jpg)

When going inside a folder with pictures, they are displayed in alphabetic order:
![inside](/docs/inside.jpg)

It recognises `jpeg|jpg|gif|png|bmp` pictures and `mp4|m4v|mpeg|mpg|avi` videos and sort them by name and pictures first.

When clicking on a picture or video, a [fancybox](http://fancyapps.com/fancybox/) is displayed. Videos are played in it with the help of the `<video>` introduced in HTML 5.
Keybinding are enabled in the navigation: <kbd>&rarr;</kbd>, <kbd>&larr;</kbd>, <kbd>ESC</kbd>

![Picture](/docs/picture.jpg)

The folder hierarchy may be as follows:
```shell
├── 03-01-dactyl
│   ├── 20220830_203541.jpg
│   ├── 20220830_203605.jpg
│   └── ...
├── 03-07_Schwarz
│   └── ...
├── 03-27-trousse
├── 2020-10-03_08-54_NOAA19_84
└── export
    ├── aImprimer
    └── clavier
```

## 👷 Installation

### Prerequisites

This gallery uses [convert from ImageMagick](http://www.imagemagick.org/script/convert.php) in order to generate thumbnails.

On debian:
```console
sudo apt install imagemagick
```

On a mac, use brew to install the `imagemagick` formulae:
```console
brew install imagemagick
```

`go-gallery` requires `golang` in version 1.14 at least.

## Building

Select an empty folder and clone the sources:
```console
git clone https://gitlab.com/coliss86/go-gallery
```

Then build it:
```console
go build -o go-gallery main.go
```

To run it:
```console
./go-gallery <config file>
```
arguments:
  * `config file`: *mandatory* path to the config file

Here is a example of this config file:
```
images=test/pictures
export=/mnt/media/photos/tag
cache=test/cache
port=9090
smallSize=900
contextRoot=/photos
byDay=false
listen=localhost
```

## 📝 Reporting Issues

  * Please report issues on [Gitlab Issue Tracker](https://gitlab.com/coliss86/go-gallery/issues).
  * In your report, please provide steps to **reproduce** the issue.
  * Before reporting:
     * Make sure you are using the latest version of `main`.
     * Check existing open issues.
  * Merge requests, documentation requests, and enhancement ideas are welcome.

## 📄 License

"Go gallery" is distributed under [GNU GPL v3](http://www.gnu.org/licenses/gpl-3.0.en.html) license, see LICENSE.

It also uses third-party libraries and programs:
  * fancyBox ([Creative Commons Attribution-NonCommercial 3.0 License](http://creativecommons.org/licenses/by-nc/3.0/)):  http://fancyapps.com/fancybox/
  * famfamfam silk icons ([Creative Commons Attribution 2.5 License](http://creativecommons.org/licenses/by/2.5/) license): http://www.famfamfam.com/lab/icons/silk/
  * woofunction icons ([GNU General Public License](http://www.gnu.org/licenses/gpl.html) license): http://www.iconarchive.com/show/woofunction-icons-by-wefunction.html
  * Tag Manager (a jQuery plugin) ([Mozilla Public License 2.0](https://www.mozilla.org/en-US/MPL/2.0/) license): https://maxfavilli.com/jquery-tag-manager
  * Icon Author: sa-ki (License: Free for personal non-commercial use): http://sa-ki.deviantart.com
  * A configuration file parser library for Go / Golang (The MIT License): ~~https://github.com/jimlawless/cfg/~~
  * Gorilla web toolkit ([BSD licensed](https://opensource.org/licenses/BSD-2-Clause)): http://www.gorillatoolkit.org/
  * Play icon (CC0 1.0 Universal (CC0 1.0) Public Domain Dedication License): http://www.iconsdb.com/caribbean-blue-icons/video-play-3-icon.html
  * jQuery Lazy (MIT License): http://jquery.eisbehr.de/lazy/
  * Icons made by [Freepik](https://www.flaticon.com/authors/freepik) from [Flaticon](https://www.flaticon.com/)
