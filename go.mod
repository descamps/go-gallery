module go-gallery

go 1.14

require (
	github.com/gorilla/context v0.0.0-20150820051245-1c83b3eabd45
	github.com/gorilla/mux v0.0.0-20151231161908-26a6070f8499
)
