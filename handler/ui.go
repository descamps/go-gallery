/*
This file is part of GO gallery.

GO gallery is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GO gallery is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GO gallery.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"regexp"
	"sort"
	"strings"
	"text/template"

	"github.com/gorilla/mux"
	"go-gallery/conf"
	"go-gallery/file"
)

type Item struct {
	Link     string
	Name     string
	LongName string
	Thumb    string
	Class    string
	Day      string
}

type Data struct {
	Title        string
	Breadcrum    []Item
	Pictures     []string
	Videos       []string
	Folders      map[string]map[string][]Item
	Months       []string
	MonthsName   map[string]string
	Folder       string
	TagsPictures map[string][]string
	Total        int
	ContextRoot  string
	ConfByDay    string
	Previous     Item
	Next         Item
}

var monthsName = map[string]string{"01": "Janvier", "02": "Février", "03": "Mars", "04": "Avril", "05": "Mai", "06": "Juin", "07": "Juillet", "08": "Août", "09": "Septembre", "10": "Octobre", "11": "Novembre", "12": "Décembre", "": "Dossiers"}

// 03-01_LongDescription
var folderShortDateRE = regexp.MustCompile("^([0-9]{4}-)?([0-9]{2})-([0-9]+)([_-](.*))?$")
var excludeRE = regexp.MustCompile("^\\..*$")
var pictureRE = regexp.MustCompile("(?i).*\\.(jpeg|jpg|gif|png|bmp)$")
var urlFolderRE = regexp.MustCompile("(.*)/([^/]*)/?")
var videoRE = regexp.MustCompile("(?i).*(mp4|m4v|mpeg|mpg|avi)$")
var BaseDir string

func RenderUI(w http.ResponseWriter, r *http.Request) {
	_ = r.ParseForm()
	vars := mux.Vars(r)
	folderS := vars["folder"]
	folder := file.PathJoin(conf.Config.Images, folderS)

	// Return a 404 if the folder doesn't exist
	info, err := os.Stat(folder)
	if err != nil && os.IsNotExist(err) || !info.IsDir() {
		http.NotFound(w, r)
		return
	}

	data := Data{}
	data.ContextRoot = conf.Config.ContextRoot
	data.ConfByDay = conf.Config.ByDay

	// title
	title := ""
	matches := urlFolderRE.FindStringSubmatch(folderS)
	if len(matches) > 0 {
		title = matches[2]
	} else {
		title = folderS
	}
	data.Title = title
	data.MonthsName = monthsName
	if strings.HasSuffix(folderS, "/") {
		data.Folder = folderS
	} else if len(folderS) > 0 {
		data.Folder = folderS + "/"
	}

	// breadcrum
	var breadcrum []Item
	if len(folderS) > 0 {
		splits := strings.Split(folderS, "/")
		for i, split := range splits {
			if len(split) > 0 {
				item := Item{}
				item.Name = split
				if i == 0 {
					item.Link = split
				} else {
					item.Link = breadcrum[i-1].Link + "/" + split
				}
				breadcrum = append(breadcrum, item)
			}
		}
		breadcrum[len(breadcrum)-1].Link = ""

		data.Breadcrum = breadcrum
	}

	// navigation to sibling folders
	if len(breadcrum) != 0 {
		parentFiles, err := ioutil.ReadDir(path.Dir(folder))
		check(err)

		parentDirs := []string{}
		for i := range parentFiles {
			if parentFiles[i].IsDir() {
				parentDirs = append(parentDirs, parentFiles[i].Name())
			}
		}
		breadcrumLen := len(breadcrum)
		currentDir := breadcrum[breadcrumLen-1].Name
		var nav string
		nav = ""
		if breadcrumLen >= 2 {
			nav = breadcrum[breadcrumLen-2].Link
		}

		for index, parentDir := range parentDirs {
			if parentDir == currentDir {
				if index > 0 {
					data.Previous.Name = parentDirs[index - 1]
					data.Previous.Link = nav + "/" + data.Previous.Name
				}
				if index + 1 < len(parentDirs) {
					data.Next.Name = parentDirs[index + 1]
					data.Next.Link = nav + "/" + data.Next.Name
				}
				break
			}
		}
	}

	// pictures
	files, err := ioutil.ReadDir(folder)
	check(err)
	data.Folders = make(map[string]map[string][]Item)
	for _, picture := range files {
		if !excludeRE.MatchString(picture.Name()) {
			if picture.IsDir() {
				manageFolder(folder, picture, data)
			} else if videoRE.MatchString(picture.Name()) {
				data.Videos = append(data.Videos, picture.Name())
			} else if pictureRE.MatchString(picture.Name()) {
				data.Pictures = append(data.Pictures, picture.Name())
			}
		}
	}

	// months
	data.Months = make([]string, len(data.Folders))
	i := 0
	for k := range data.Folders {
		data.Months[i] = k
		i++
	}
	sort.Strings(data.Months)

	// tags
	data.TagsPictures = make(map[string][]string)
	tags := tagList()
	for _, t := range tags {
		data.TagsPictures[t] = tagListPictures(t)
	}

	data.Total = len(data.Videos) + len(data.Pictures)

	// final generation
	var templates = template.Must(template.ParseFiles(BaseDir + "/template/gallery.tmpl"))
	err = templates.Execute(w, data)
	check(err)
}

func manageFolder(parent string, folder os.FileInfo, data Data) {
	item := Item{}
	month := ""
	day := "0"
	item.Link = folder.Name()
	matches := folderShortDateRE.FindStringSubmatch(item.Link)
	if len(matches) > 0 {
		month = matches[2]
		if conf.Config.ByDay == "true" {
			day = matches[3]
			item.Day = day
			item.Class = "folder-long"
			item.Name = matches[5]
		} else {
			item.Class = "folder-short"
			item.Name = matches[3]
		}
		// matches :
		// [2020-09-04_21-07_NOAA15_51 2020- 09 04 _21-07_NOAA15_51 21-07_NOAA15_51]
		// ou
		// [03-27-trousse  03 27 -trousse trousse]
		// ou
		// [03-27  03 27  ]
		if len(matches[1]) > 3 { // 2020-
			item.LongName = matches[5]
		} else if matches[5] != "" {
			item.LongName = matches[5]
		} else {
			item.LongName = matches[3]
		}

		if item.LongName == "" {
			item.LongName = item.Link
		}
	} else {
		// regexp do not match
		item.Name = item.Link
		item.LongName = item.Name
		if len(item.Name) > 7 {
			item.Class = "folder-long"
		} else {
			item.Class = "folder-normal"
		}
	}

	meta := readMeta(file.PathJoin(parent, item.Link))
	if meta != nil {
		item.LongName = strings.Replace(meta["title"], " ", "&nbsp;", -1)
	}

	// thumb folder
	files, err := ioutil.ReadDir(file.PathJoin(parent, folder.Name()))
	check(err)

	for _, fileInfo := range files {
		if !fileInfo.IsDir() && pictureRE.MatchString(fileInfo.Name()) {
			item.Thumb = fileInfo.Name()
			item.Class += " folder-thumb"
			break
		}
	}

	// store value
	_, exists := data.Folders[month]
	if !exists {
		data.Folders[month] = make(map[string][]Item)
	}

	vDay, exists := data.Folders[month][day]
	if !exists {
		data.Folders[month][day] = make([]Item, 5)
	}

	data.Folders[month][day] = append(vDay, item)
}

func readMeta(folder string) map[string]string {
	path := file.PathJoin(folder, "meta.properties")
	info, err := os.Stat(path)
	if err != nil || info.IsDir() {
		return nil
	}
	meta := make(map[string]string)
	_ = conf.Load(path, meta)
	return meta
}
